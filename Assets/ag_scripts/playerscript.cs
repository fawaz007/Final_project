﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnitySampleAssets.CrossPlatformInput;


public class playerscript : MonoBehaviour
{
    Rigidbody playerRigidbody;
    Vector3 movement;
    [SerializeField]
    float SPEED=5.0f;

    void Awake()
    {
        playerRigidbody = GetComponent<Rigidbody>();

    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        float v = CrossPlatformInputManager.GetAxis("Vertical");
        Move(h, v);
    }
    void Move(float h, float v)
    {
        movement.Set(h, 0.0f, v);
        movement = movement.normalized * Time.deltaTime*SPEED;
        playerRigidbody.MovePosition(transform.position + movement);
    }
}
